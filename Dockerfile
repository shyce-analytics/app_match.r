FROM ubuntu:jammy
WORKDIR /app

RUN apt-get update  
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN apt-get install -y git unzip build-essential curl libxml2-dev libpoppler-cpp-dev r-base r-base-dev  
RUN apt-get install -y --no-install-recommends r-cran-stringi r-cran-getopt r-cran-tm r-cran-snowballc r-cran-pheatmap r-cran-jsonlite r-cran-data.table  
RUN apt-get clean  


COPY . .
RUN curl -O https://f-droid.org/repo/index-v1.jar
RUN unzip -j -o "index-v1.jar" "index-v1.json" -d "./"

RUN Rscript app_match.R -n 20 -d out

#RUN yarn install --production
#CMD ["node", "src/index.js"]
#EXPOSE 3000