#!/bin/bash

if  test `find -mmin -60 -size +64k -name "index-v1.jar"`
then
  echo ./index-v1.jar is non-empty and seems recent, skipping download
else
  curl -O https://f-droid.org/repo/index-v1.jar
  unzip -j -o "index-v1.jar" "index-v1.json" -d "./"
fi

./app_match.R $*

# Plot all diagrams:
# Something similar might be useful for TWIF ("This week in f-droid")
# With --mtime added the diagrams get the date when they were added,
# so that newest additions can be found by sorting by file date.
# Expect about 1.5 hours runtime (core i7 2600) for ~12000 diagrams.
# ./app_match.R --mtime added -n 25 -m 0xff -d out

# Plot one type of diagrams and only plot recently added Apps:
# ./app_match.R --mtime added -m 0x02 --plotselectadded 14 -d out

# Output command line options:
# ./app_match.R --help
